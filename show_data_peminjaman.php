<?php 
    $DB_NAME        = "pinjambarang";
    $DB_USER        = "root";
    $DB_PASS        = "";
    $DB_SERVER_LOC  = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = mysqli_connect($DB_SERVER_LOC, $DB_USER, $DB_PASS, $DB_NAME);
        $sql = " SELECT p.id_pinjam, p.nama_pinjam, p.no_hp, p.tgl_pinjam, b.nama_barang, 
                p.status, concat('http://192.168.43.81/pinjambarang/image/', ktp) as url
                FROM pinjam p, barang b
                WHERE p.kode_barang = b.kode_barang order by tgl_pinjam desc";
        $result = mysqli_query($conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_pjm = array();
            while ($pjm = mysqli_fetch_assoc($result)) {
                array_push($data_pjm, $pjm);
            }
            echo json_encode($data_pjm);
        }
    }
?>