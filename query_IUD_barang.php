<?php
$DB_NAME = "pinjambarang";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $conn = mysqli_connect($DB_SERVER_LOC, $DB_USER, $DB_PASS, $DB_NAME);
    $mode = $_POST['mode'];
    $response = array(); $response['kode'] = '000';
    switch ($mode) {
        case "insert":
            $kode = $_POST['kode_barang'];
            $nama = $_POST['nama_barang'];
            $nama_kategori = $_POST['nama_kategori'];
            $imstr = $_POST['image'];
            $file = $_POST['file'];
            $path = "images/";

            $sql = "select kode_kategori from kategori where nama_kategori='$nama_kategori'";
            $result = mysqli_query($conn, $sql);
            if (mysqli_num_rows($result) > 0) {
                $data = mysqli_fetch_assoc($result);
                $kode_kategori = $data['kode_kategori'];

                $sql = "insert into barang(kode_barang, nama_barang, kode_kategori, photos) values(
                    '$kode','$nama',$kode_kategori,'$file'
                )";
                $result = mysqli_query($conn, $sql);
                if ($result) {
                    if (file_put_contents($path.$file, base64_decode($imstr)) == false) {
                        $sql = "delete from barang where kode_barang = '$kode'";
                        mysqli_query($conn, $sql);
                        $response['kode'] = "111";
                        echo json_encode($response);
                        exit();
                    } else {
                        echo json_encode($response); //insert data sukses semua
                        exit();
                    }
                } else {
                    $response['kode'] = "111";
                    echo json_encode($response);
                    exit();
                }
            }
            break;

        case "update":
            $kode = $_POST['kode_barang'];
            $nama = $_POST['nama_barang'];
            $nama_kategori = $_POST['nama_kategori'];
            $imstr = $_POST['image'];
            $file = $_POST['file'];
            $path = "images/";

            $sql = "select kode_kategori from kategori where nama_kategori='$nama_kategori'";
            $result = mysqli_query($conn, $sql);
                if (mysqli_num_rows($result) > 0) {
                    $data = mysqli_fetch_assoc($result);
                    $kode_kategori = $data['kode_kategori'];

                $sql = "";
                if ($imstr == "") {
                    $sql = "update barang set nama_barang='$nama', kode_kategori=$kode_kategori
                            where kode_barang='$kode'";
                    $result = mysqli_query($conn, $sql);
                    if ($result) {
                        echo json_encode($response); //update data sukses semua
                        exit();
                    } else {
                        $response['kode'] = "111";
                        echo json_encode($response); exit();
                    }
                } else {
                        if (file_put_contents($path . $file, base64_decode($imstr)) == false) {
                            $response['kode'] = "111";
                            echo json_encode($response);
                            exit();
                        } else {
                            $sql = "update barang set nama_barang='$nama',kode_kategori=$kode_kategori, photos='$file'
                            where kode_barang='$kode'";
                            $result = mysqli_query($conn, $sql);
                            if ($result) {
                                echo json_encode($response); //update data sukses semua
                                exit();
                            } else {
                                $response['kode'] = "111";
                                echo json_encode($response);
                                exit();
                            }
                        }
                    }
            }
            break;

        case "delete":
            $kode = $_POST['kode_barang'];

            $sql = "select photos from barang where kode_barang='$kode'";
            $result = mysqli_query($conn, $sql);
                if ($result) {
                    if (mysqli_num_rows($result) > 0) {
                        $data = mysqli_fetch_assoc($result);
                        $photos = $data['photos'];
                        $path = "images/";
                        unlink($path.$photos);
                    }

                    $sql = "delete from barang where kode_barang='$kode'";
                    $result = mysqli_query($conn, $sql);
                    if ($result) {
                        echo json_encode($response); //delete data sukses semua
                        exit();
                    }  else {
                        $response['kode'] = "111";
                        echo json_encode($response);
                        exit();
                    }
            }
            break;
    }
}
?>