<?php
$DB_NAME = "pinjambarang";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $conn = mysqli_connect($DB_SERVER_LOC, $DB_USER, $DB_PASS, $DB_NAME);
    $sql = "select nama_barang from barang order by nama_barang asc";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json; charset=UTF-8");

        $nama_barang = array();
        while ($nm_barang = mysqli_fetch_assoc($result)) {
            array_push($nama_barang, $nm_barang);
        }
        echo json_encode($nama_barang);
    }
}
?>