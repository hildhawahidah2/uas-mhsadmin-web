/*
SQLyog Professional v13.1.1 (64 bit)
MySQL - 10.1.38-MariaDB : Database - pinjambarang
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `pinjambarang`;

/*Table structure for table `barang` */

DROP TABLE IF EXISTS `barang`;

CREATE TABLE `barang` (
  `kode_barang` varchar(20) NOT NULL,
  `nama_barang` varchar(100) DEFAULT NULL,
  `kode_kategori` int(3) DEFAULT NULL,
  `photos` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `barang` */

insert  into `barang`(`kode_barang`,`nama_barang`,`kode_kategori`,`photos`) values 
('Kblroll01','Meiko3 Lubang',3,'QRMeiko3 Lubang20200518070950.jpg'),
('Kblroll02','Meiko5 Lubang',3,'QRMeiko5 Lubang20200518071017.jpg'),
('KpsAngn01','Gmc Tornado',1,'QRGmc Tornado20200518070637.jpg'),
('KpsAngn02','Regency DLX',1,'QRRegency DLX20200518070731.jpg'),
('KpsAngn03','Regency Deluxe',1,'QRRegency Deluxe20200518070812.jpg'),
('Pryktr01','Epson01',2,'QREpson0120200518070421.jpg'),
('Pryktr02','Epson02',2,'QREpson0220200518070459.jpg');

/*Table structure for table `kategori` */

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `kode_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`kode_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `kategori` */

insert  into `kategori`(`kode_kategori`,`nama_kategori`) values 
(1,'Kipas Angin'),
(2,'Proyektor'),
(3,'Kabel Roll');

/*Table structure for table `pinjam` */

DROP TABLE IF EXISTS `pinjam`;

CREATE TABLE `pinjam` (
  `id_pinjam` varchar(20) NOT NULL,
  `nama_pinjam` varchar(100) DEFAULT NULL,
  `no_hp` varchar(14) DEFAULT NULL,
  `ktp` varchar(100) DEFAULT NULL,
  `tgl_pinjam` date DEFAULT NULL,
  `kode_barang` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_pinjam`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pinjam` */

insert  into `pinjam`(`id_pinjam`,`nama_pinjam`,`no_hp`,`ktp`,`tgl_pinjam`,`kode_barang`,`status`) values 
('6b6Ei1','rizia','086562782666','rizia.jpg','2020-05-18','KpsAngn01','Dipinjam'),
('6BJNA6','bisma','087653672919','bisma.jpg','2020-05-18','KpsAngn02','Dipinjam'),
('9voAKO','ayupi','087653728963','ayupi.jpg','2020-05-18','Pryktr02','Dipinjam'),
('fjDIy3','gilang','086772651764','gilang.jpg','2020-05-18','Kblroll02','Dipinjam'),
('jgjbTg','hildha ','087774532655','hildha .jpg','2020-05-18','Pryktr01','Dipinjam'),
('pdkcXm','indah','087653257865','indah.jpg','2020-05-18','KpsAngn03','Dipinjam'),
('PFSxK4','jonshon','087637267186','jonshon.jpg','2020-05-18','Kblroll01','Dipinjam');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
