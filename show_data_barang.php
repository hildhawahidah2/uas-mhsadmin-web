<?php 
    $DB_NAME        = "pinjambarang";
    $DB_USER        = "root";
    $DB_PASS        = "";
    $DB_SERVER_LOC  = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = mysqli_connect($DB_SERVER_LOC, $DB_USER, $DB_PASS, $DB_NAME);
        $sql = " SELECT b.kode_barang, b.nama_barang,
                k.nama_kategori, concat('http://192.168.43.81/pinjambarang/images/', photos) as url
                FROM barang b, kategori k
                WHERE b.kode_kategori = k.kode_kategori";
        $result = mysqli_query($conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_brg = array();
            while ($brg = mysqli_fetch_assoc($result)) {
                array_push($data_brg, $brg);
            }
            echo json_encode($data_brg);
        }
    }
?>